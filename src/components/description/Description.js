import {memo} from 'react';
import {Fieldset} from 'primereact/fieldset';
import {parsePrice, parseWeight} from '../../helpers/product';

const labels = {
    brand: 'Brand',
    model: 'Model',
    price: 'Price',
    dimentions: 'Dimentions',
    weight: 'Weight'
};

const valueParser = {
    price: parsePrice,
    weight: parseWeight 
};

const Description = ({product}) => {
    return <Fieldset legend='Description'>  
        {Object.entries(labels).map(([key, value]) => <div key={key} className='grid'>
            <div className='col-3 text-900 font-medium'>{value}:</div>
            <div className='col-9'>{valueParser[key]?.(product) ?? product?.[key]}</div>
        </div>)}   
    </Fieldset>
}

export default memo(Description);