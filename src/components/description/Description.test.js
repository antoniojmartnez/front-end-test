import {render, screen} from '@testing-library/react';
import Description from './Description';

test('Render Description', () => {
  render(<Description />);
  const element = screen.getByText(/Description/i);
  expect(element).toBeInTheDocument();
});