import {useCallback, useMemo, memo} from 'react';
import {NavLink, useMatch} from "react-router-dom";
import {Badge} from 'primereact/badge';
import {useSelector} from 'react-redux';
import {useGetProductsQuery} from "../../services/frontend-test";
import {parseDisplayName} from '../../helpers/product';

const Header = () => {
    const match = useMatch('/product/:id');
    const {data: products = []} = useGetProductsQuery();
    const cartCount = useSelector(state => state.cart.count);

    const product = useMemo(
        () => match ? products.find(p => p.id === match.params.id) : null,
        [match, products]
    );
    
    const selectLinkClassNames = useCallback(
        ({isActive}) => 
            isActive ? 'text-900 line-height-3' : 'text-500 no-underline line-height-3 cursor-pointer',
        []
    );

    return <div className='grid'>
            <div className='col'>
                <ul className="list-none p-0 m-0 flex align-items-center font-medium text-2xl mb-3">
                    <li className='hidden font-bold text-3xl md:inline-block'>
                        <NavLink to='/' className={selectLinkClassNames}>
                            Front-End Test
                        </NavLink>
                    </li>
                    <li className="px-2">
                        <i className="pi pi-angle-right text-500 line-height-3"></i>
                    </li>
                    <li>
                        <NavLink end to='/product' className={selectLinkClassNames}>
                            Products
                        </NavLink>
                    </li>
                    {match && <>
                        <li className="px-2">
                            <i className="pi pi-angle-right text-500 line-height-3"></i>
                        </li>
                        <li>
                            <NavLink to={`/product/${match.params.id}`} className={selectLinkClassNames}>
                                {product ? parseDisplayName(product) : match.params.id}
                            </NavLink>
                        </li>
                    </>}
                </ul>
            </div>
            <div className='col-fixed flex align-items-center justify-content-end' style={{width: '100px'}}>
                <i className="pi pi-shopping-cart mr-4 p-text-secondary p-overlay-badge" style={{fontSize: '2rem'}}>
                    <Badge value={String(cartCount)}></Badge>
                </i> 
            </div>
        </div>;
}

export default memo(Header);