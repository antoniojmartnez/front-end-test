import {useState, useMemo, useEffect, useCallback, memo} from 'react';
import {Fieldset} from 'primereact/fieldset';
import {Dropdown} from 'primereact/dropdown';
import {Button} from 'primereact/button';

const optionsLabels = {
    colors: 'Color',
    storages: 'Storage'
};

const Actions = ({options, isLoading, onAdd}) => {
    const [selectedOptions, setSelectedOptions] = useState({});

    const optionsEntries = useMemo(() => Object.entries(options ?? []), [options]);

    useEffect(() => {
        const defaultSelectedOpts = {};
        optionsEntries.forEach(([key, value]) => {
            if (value?.length === 1) {
                defaultSelectedOpts[key] = value[0]?.code;
            }
        });
        setSelectedOptions(prevSelectedOptions => ({
            ...prevSelectedOptions,
            ...defaultSelectedOpts
        }));
    }, [optionsEntries]);

    const handleOptionChange = useCallback(({target}) => {
        const {name, value} = target;
        setSelectedOptions(prevSelectedOptions => ({
            ...prevSelectedOptions,
            [name]: value
        }));
    }, []);

    const handleAddClick = useCallback(e => {
        if (onAdd) {
            onAdd(selectedOptions);
        }
    }, [onAdd, selectedOptions]);

    const areAllOptionsSelected = useMemo(
        () => Object.values(selectedOptions).filter(Boolean).length === optionsEntries.length,
        [selectedOptions, optionsEntries]
    );

    return <Fieldset legend='Actions'>
        <div className='grid'>
            {optionsEntries.map(([key, value]) => <>
                <div className='col-3 text-900 font-medium flex align-items-center'>
                    {optionsLabels[key] ?? key}:
                </div>
                <div className='col-9'>
                    <Dropdown
                        name={key}
                        options={value}
                        optionValue='code'
                        optionLabel='name'
                        style={{width: '100%'}}
                        value={selectedOptions?.[key]}
                        onChange={handleOptionChange}
                        placeholder={'Select an option...'}
                    />
                </div>
            </>)}
            <div className='col-3'></div>
            <div className='col-9'>
                <Button
                    label='Add'
                    icon='pi pi-shopping-cart'
                    onClick={handleAddClick}
                    disabled={!areAllOptionsSelected}
                    loading={isLoading}
                />
            </div>
        </div>
    </Fieldset>;
};

export default memo(Actions);