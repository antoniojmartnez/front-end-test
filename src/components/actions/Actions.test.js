import {render, screen} from '@testing-library/react';
import Actions from './Actions';

test('Render Actions', () => {
  render(<Actions />);
  const element = screen.getByText(/Actions/i);
  expect(element).toBeInTheDocument();
});