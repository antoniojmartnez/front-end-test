import {useCallback, memo} from 'react';
import {parseDisplayName, parsePrice} from '../../helpers/product';
import Image from '../image/Image';

const Item = ({data, onClick}) => {
    const handleClick = useCallback(e => {
        if (onClick) {
            onClick(data.id, e);
        }
    }, [data, onClick]);

    const diplayName = parseDisplayName(data);

    return <div className='grid m-3 p-3 cursor-pointer border-1 border-round border-gray-300 hover:border-primary' onClick={handleClick}>
        <div className='col-12 flex align-items-center justify-content-center'>
            <Image src={data.imgUrl} alt={diplayName} className='h-15rem'/>
        </div>
        <div className='col-12'>
            <div className='grid font-medium text-lg'>
                <div className='col flex align-items-center justify-content-start'>
                    {diplayName}
                </div>
                <div className='col-fixed flex align-items-center justify-content-end'>
                    {data.price ? parsePrice(data) : 'N/A'}
                </div>
            </div>
        </div>
    </div>;
}

export default memo(Item);