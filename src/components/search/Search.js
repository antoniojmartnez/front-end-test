import {memo} from 'react';

const Search = ({value, onChange}) => {
    return <span className='p-input-icon-left'>
        <i className="pi pi-search" />
        <input type='search' className='p-inputtext p-component' {...{value, onChange}} />
    </span>
}

export default memo(Search);