const Image = ({src, alt, className}) => {
    return <img {...{src, className, alt}} />;
}

export default Image;