const parsePropsTemplate = (product = {}, template = '') => 
    Object.entries(product).reduce(
        (acc, [key, value]) => acc.replace(`{${key}}`, value),
        template
    );


export const parseDisplayName = product => {
    if (product?.brand && product?.model) {
        return parsePropsTemplate(product, '{brand} - {model}');
    }
    
    if (product?.brand) {
        return product?.brand;
    }

    if (product?.model) {
        return product?.model;
    }

    return 'Unknown';
};

export const parsePrice = product => {
    if (!product?.price) {
        return 'Not available';
    }
    return parsePropsTemplate(product, '{price} €');
};

export const parseWeight = product => {
    if (!product?.weight) {
        return 'Not available';
    }
    return parsePropsTemplate(product, '{weight} g');
}