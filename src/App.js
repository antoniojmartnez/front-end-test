import 'primereact/resources/themes/lara-light-indigo/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import {Route, Routes} from 'react-router-dom';
import {ProductListPage, ProductDetailPage} from './pages';
import {Header} from './components';

const App = () => {
    return <div className='px-4 md:px-6 lg:px-8'>
        <Header />
        <Routes>
            <Route path='product' element={<ProductListPage />} />
            <Route path='product/:id' element={<ProductDetailPage />} />
        </Routes>
    </div>;
};

export default App;
