import {useEffect, useRef, useState} from 'react';

const useDebounce = (value, time = 250) => {
    const timeoutRef = useRef();

    const [debouncedValue, setDebouncedValue] = useState();

    useEffect(() => {
        clearTimeout(timeoutRef.current);
        timeoutRef.current = setTimeout(() => {
            setDebouncedValue(value)
        }, time);

        return () => {
            clearTimeout(timeoutRef.current);
        }
    }, [value, time]);

    return debouncedValue;
}

export default useDebounce;