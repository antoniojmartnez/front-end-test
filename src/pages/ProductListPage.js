import {useState, useCallback, useMemo, memo} from 'react';
import {NavLink} from 'react-router-dom';
import {Item, Search} from '../components'
import useDebounce from '../hooks/useDebounce';
import {useGetProductsQuery} from '../services/frontend-test';

const ProductListPage = () => {
    const {data: products, error, isLoading} = useGetProductsQuery();

    const [searchText, setSearchText] = useState('');

    const debouncedSearchText = useDebounce(searchText);

    const handleSearchChange = useCallback(e => {
        setSearchText(e.target.value);
    }, []);

    const filteredProducts = useMemo(() => products?.filter(
        p => p.brand.toLowerCase().includes(debouncedSearchText?.toLowerCase()) 
            || p.model.toLowerCase().includes(debouncedSearchText?.toLowerCase())
    ), [products, debouncedSearchText]);

    if (isLoading) {
        return 'Loading...';
    }

    if (error) {
        return 'ERROR, try again!';
    }

    return <div className='grid'>
        <div className='col-12 flex justify-content-end'>
            <Search value={searchText} onChange={handleSearchChange}/>
        </div>
        <div className='col-12'>
            <div className={'grid'}>
                {filteredProducts?.map(p => <div className={'col-12 md:col-6 lg:col-3'}>
                    <NavLink to={`/product/${p.id}`} className='no-underline text-900'>
                        <Item data={p} />
                    </NavLink>
                </div>)}
            </div>
        </div>
    </div>;
};

export default memo(ProductListPage);