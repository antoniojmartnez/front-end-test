import {useEffect, useCallback, memo} from 'react';
import {useDispatch} from 'react-redux';
import {useParams} from "react-router-dom";
import {useGetProductByIdQuery, useUpdateCartMutation} from '../services/frontend-test';
import {setCount as setCartCount} from '../store/slices/cart';
import {parseDisplayName} from '../helpers/product';
import Image from '../components/image/Image';
import Description from '../components/description/Description';
import Actions from '../components/actions/Actions';

const ProductDetailPage = () => {
    const dispatch = useDispatch();
    const {id} = useParams();
    const {data: product, error, isLoading} = useGetProductByIdQuery(id);
    const [updateCart, updateCartState] = useUpdateCartMutation();

    useEffect(() => {
        if (updateCartState.isSuccess) {
            dispatch(setCartCount(updateCartState.data.count));
        }
    }, [updateCartState, dispatch]);

    const handleAdd = useCallback(selectedOptions => {
        updateCart({
            id,
            colorCode: selectedOptions?.['colors'],
            storageCode: selectedOptions?.['storages']
        });
    }, [updateCart, id]);

    if (isLoading) {
        return 'Loading...';
    }

    if (error) {
        return 'ERROR, try again!';
    }

    return <div className='grid'>
        <div className='col-12 lg:col-6 flex justify-content-center'>
            <Image
                src={product.imgUrl}
                alt={parseDisplayName(product)}
                className='h-30rem' 
            />
        </div>
        <div className='col-12 lg:col-6'>
            <div className='grid'>
                <div className='col-12'>
                    <Description
                        product={product}
                    />
                </div>
                <div className='col-12'>
                    <Actions 
                        options={product?.options}
                        isLoading={updateCartState.isLoading}
                        onAdd={handleAdd}
                    />
                </div>
            </div>
        </div>
    </div>;
}

export default memo(ProductDetailPage);