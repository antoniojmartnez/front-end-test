import {configureStore} from '@reduxjs/toolkit';
import {setupListeners} from '@reduxjs/toolkit/query';
import {frontendTestAPI} from '../services/frontend-test';
import cartReducer from './slices/cart';

export const store = configureStore({
  reducer: {
      [frontendTestAPI.reducerPath]: frontendTestAPI.reducer,
      cart: cartReducer
  },
  middleware: getDefaultMiddleware => 
    getDefaultMiddleware().concat(frontendTestAPI.middleware)
})

setupListeners(store.dispatch);