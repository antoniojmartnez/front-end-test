import {createSlice} from '@reduxjs/toolkit'

const initialState = {
  count: 0,
}

export const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    setCount: (state, action) => {
      state.count = action.payload
    }
  },
})

export const {setCount} = cartSlice.actions

export default cartSlice.reducer