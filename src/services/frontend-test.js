import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react'

export const frontendTestAPI = createApi({
    reducerPath: 'frontendTestAPI',
    baseQuery: fetchBaseQuery({
        baseUrl: 'https://front-test-api.herokuapp.com/api/'
    }),
    endpoints: builder => ({
        getProducts: builder.query({
            query: () => 'product'
        }),
        getProductById: builder.query({
            query: id => `product/${id}`,
        }),
        updateCart: builder.mutation({
            query: body => ({
                url: 'cart',
                method: 'POST',
                body
            })
        })
    }),
})

export const {useGetProductsQuery, useGetProductByIdQuery, useUpdateCartMutation} = frontendTestAPI